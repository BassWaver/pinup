## Pinup

A lightweight notepad that follows you

Written with electron for Macos

#### Getting Started

Save a file using `Command S`,

Load a file using `Command O`

Open an instance using `Command N`

Close an instance using `Command W`
